DROP DATABASE IF EXISTS cochescircuitosdb;
CREATE DATABASE cochescircuitosdb;
USE cochescircuitosdb;

CREATE TABLE `Coches` (`id` INT NOT NULL AUTO_INCREMENT,
                            `nombre` VARCHAR(50) NOT NULL DEFAULT '',
                             `ganancia` INT NOT NULL DEFAULT 0,
                             PRIMARY KEY (`id`),
                             UNIQUE INDEX `UNIQUE_KEY` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Circuitos` (
                          `id` INT NOT NULL AUTO_INCREMENT,
                          `nombre` VARCHAR(50) NOT NULL DEFAULT '',
                          `ciudad` VARCHAR(50) NOT NULL DEFAULT '',
                          `pais` VARCHAR(50) NOT NULL DEFAULT '',
                          `numeroVueltas` INT NOT NULL DEFAULT 0,
                          `longitud` INT NOT NULL DEFAULT 0,
                          `numeroCurvas` INT NOT NULL DEFAULT 0,

                          PRIMARY KEY (`id`),
                          UNIQUE INDEX `UNIQUE_KEY` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;