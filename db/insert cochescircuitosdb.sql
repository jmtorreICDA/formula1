DELETE FROM `cochescircuitosdb`.`Coches`;
DELETE FROM `cochescircuitosdb`.`Circuitos`;

ALTER TABLE `cochescircuitosdb`.`Coches` AUTO_INCREMENT = 1 ;
INSERT INTO `cochescircuitosdb`.`Coches` VALUES (1, 'Simca 1000', 4);
INSERT INTO `cochescircuitosdb`.`Coches` VALUES (2, 'Seat 124 Sport 1800', 5);
INSERT INTO `cochescircuitosdb`.`Coches` VALUES (3, 'Renault 5 Copa Turbo', 7);
INSERT INTO `cochescircuitosdb`.`Coches` VALUES (4, 'Peugout 205 Rally', 9);


ALTER TABLE `cochescircuitosdb`.`Circuitos` AUTO_INCREMENT = 1 ;
INSERT INTO `cochescircuitosdb`.`Circuitos` VALUES (1, 'Jarama', 'Madrid','España',50,5000,15);
INSERT INTO `cochescircuitosdb`.`Circuitos` VALUES  (2, 'Nurburgring', 'Nurburg','Alemania',70,6000,10);
INSERT INTO `cochescircuitosdb`.`Circuitos` VALUES (3, 'Monaco', 'Monaco','Monaco',60,7000,17);




