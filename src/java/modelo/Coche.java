/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author jmdel
 */
public class Coche {
    private int id;
    private String nombre;
    private int ganancia;
    
    public Coche(int id, String nombre,int ganancia){
     this.id=id;
     this.nombre=nombre;
     this.ganancia=ganancia;
    }
    
    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id=id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getGanancia() {
        return ganancia;
    }

    public void setGanancia(int ganancia) {
        this.ganancia = ganancia;
    }
    
}
