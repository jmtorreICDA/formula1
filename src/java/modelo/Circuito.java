/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author jmdel
 */
public class Circuito {
    private int id;
    private String nombre;
    private String ciudad;
    private String pais;
    private Integer vueltas;
    private Integer longitud;
    private Integer curvas;
    
    public Circuito(int id, String nombre,String ciudad, String pais,int vueltas,int longitud, int curvas){
     this.id=id;
     this.nombre=nombre;
     this.ciudad=ciudad;
     this.pais= pais;
     this.vueltas = vueltas;
     this.longitud = longitud;
     this.curvas=curvas;
     
    }
     public int getId(){
        return id;
    }
    public void setId(int id){
        this.id=id;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCiudad() {
        return ciudad;
    }
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }
    public void setPais(String pais) {
        this.pais = pais;
    }

    public int getVueltas() {
        return vueltas;
    }
    public void setVueltas(int vueltas) {
        this.vueltas= vueltas;
    }

    public int getLongitud() {
        return longitud;
    }
    public void setLongitud(int longitud) {
        this.longitud= longitud;
    }

    public int getCurvas() {
        return curvas;
    }
    public void setCurvas(int curvas) {
        this.curvas= curvas;
    }
    
}
