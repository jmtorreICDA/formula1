/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author jmdel
 */
import config.Conexion;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Connection;

public class ModeloDatos {  
    
    Connection conexion;
    private Statement set;
    private ResultSet rs; //antiguo

    
    //El constructor inicia la conexión con la bbdd
    public ModeloDatos() {
        
        Conexion con =new Conexion();
        conexion = con.getConexion();
        
    }
    //métodos de gestión de cochesa
    public List<Coche> mostrarCoches (){
        PreparedStatement ps;
         ResultSet rs;
       List<Coche> listaCoches = new ArrayList();
       try{
           ps = conexion.prepareStatement("SELECT id,nombre,ganancia FROM coches");
           rs = ps.executeQuery();
           while (rs.next()){
               int id=rs.getInt("id");
               String nombre = rs.getString("nombre");
               int ganancia = rs.getInt("ganancia");
               Coche coche= new Coche(id,nombre,ganancia);
               listaCoches.add(coche);         
            }
           return listaCoches;
       }catch(SQLException e){
           System.out.print(e.toString());
           return null;
       }
    }
    public Coche buscarCoche (int idCoche){
        PreparedStatement ps;
        ResultSet rs;
        Coche coche=null;
       
       try{
           ps = conexion.prepareStatement("SELECT id,nombre,ganancia FROM coches WHERE id=?");
           ps.setInt(1,idCoche);
           rs = ps.executeQuery();
           while (rs.next()){
               int id=rs.getInt("id");
               String nombre = rs.getString("nombre");
               int ganancia = rs.getInt("ganancia");
               coche= new Coche(id,nombre,ganancia);                       
            }
           return coche;
       }catch(SQLException e){
           System.out.print(e.toString());
           return null;
       }
    }
    public boolean insertarCoche (Coche coche){
        PreparedStatement ps;               
       try{
           ps = conexion.prepareStatement("INSERT INTO coches (nombre,ganancia) VALUES (?,?)");
           ps.setString(1,coche.getNombre());
           ps.setInt(2,coche.getGanancia());
           ps.execute();
           
           return true;
       }catch(SQLException e){
           System.out.print(e.toString());
           return false;
       }
    }
    public boolean actualizarCoche (Coche coche){
        PreparedStatement ps;               
       try{
           ps = conexion.prepareStatement("UPDATE coches SET nombre=?, ganancia=? WHERE id=?");
           ps.setString(1,coche.getNombre());
           ps.setInt(2,coche.getGanancia());
           ps.setInt(3,coche.getId());
           ps.execute();
           
           return true;
       }catch(SQLException e){
           System.out.print(e.toString());
           return false;
       }
    }
    public boolean eliminarCoche (int idCoche){
        PreparedStatement ps;               
       try{
           ps = conexion.prepareStatement("DELETE FROM coches WHERE id=?");
           ps.setInt(1,idCoche);
           ps.execute();
           
           return true;
       }catch(SQLException e){
           System.out.print(e.toString());
           return false;
       }
    }
    //Metodos de gestión de circuitos
   public List<Circuito> mostrarCicuitos (){
        PreparedStatement ps;
         ResultSet rs;
       List<Circuito> listaCircuitos = new ArrayList();
       try{
           ps = conexion.prepareStatement("SELECT id,nombre,ciudad,pais,numeroVueltas,longitud,numeroCurvas FROM circuitos");
           rs = ps.executeQuery();
           while (rs.next()){
               int id=rs.getInt("id");
               String nombre = rs.getString("nombre");
               String ciudad = rs.getString("ciudad");
               String pais = rs.getString("pais");
               int vueltas = rs.getInt("numeroVueltas");
               int longitud = rs.getInt("longitud");
               int curvas = rs.getInt("numeroCurvas");
               Circuito circuito= new Circuito(id,nombre,ciudad,pais,vueltas,longitud,curvas);
               listaCircuitos.add(circuito);         
            }
           return listaCircuitos;
       }catch(SQLException e){
           System.out.print(e.toString());
           return null;
       }
    } 
   public Circuito buscarCircuito (int idCircuito){
        PreparedStatement ps;
        ResultSet rs;
        Circuito circuito=null;
       
       try{
           ps = conexion.prepareStatement("SELECT id,nombre,ciudad,pais,numeroVueltas,longitud,numeroCurvas FROM circuitos WHERE id=?");
           ps.setInt(1,idCircuito);
           rs = ps.executeQuery();
           while (rs.next()){
               int id=rs.getInt("id");
               String nombre = rs.getString("nombre");
               String ciudad = rs.getString("ciudad");
               String pais = rs.getString("pais");
               int vueltas = rs.getInt("numeroVueltas");
               int longitud = rs.getInt("longitud");
               int curvas = rs.getInt("numeroCurvas");
               circuito= new Circuito(id,nombre,ciudad,pais,vueltas,longitud,curvas);                      
            }
           return circuito;
       }catch(SQLException e){
           System.out.print(e.toString());
           return null;
       }
    }
   public boolean insertarCircuito (Circuito circuito){
    PreparedStatement ps;               
       try{
           ps = conexion.prepareStatement("INSERT INTO circuitos (nombre,ciudad,pais,numeroVueltas,longitud,numeroCurvas) VALUES (?,?,?,?,?,?)");
           ps.setString(1,circuito.getNombre());           
           ps.setString(2,circuito.getCiudad());
           ps.setString(3,circuito.getPais());
           ps.setInt(4,circuito.getVueltas());
           ps.setInt(5,circuito.getLongitud());
           ps.setInt(6,circuito.getCurvas());
           ps.execute();           
           return true;
       }catch(SQLException e){
           System.out.print(e.toString());
           return false;
       }
    }
   public boolean actualizarCircuito (Circuito circuito){
    PreparedStatement ps;               
       try{
           ps = conexion.prepareStatement("UPDATE circuitos SET nombre=?, ciudad=?, pais=? ,numeroVueltas=? ,longitud=? ,numeroCurvas=? WHERE id=?");
           ps.setString(1,circuito.getNombre());           
           ps.setString(2,circuito.getCiudad());
           ps.setString(3,circuito.getPais());
           ps.setInt(4,circuito.getVueltas());
           ps.setInt(5,circuito.getLongitud());
           ps.setInt(6,circuito.getCurvas());
           ps.setInt(7,circuito.getId());
           ps.execute();           
           return true;
       }catch(SQLException e){
           System.out.print(e.toString());
           return false;
       }
    }
   public boolean eliminarCircuito (int idCircuito){
        PreparedStatement ps;               
       try{
           ps = conexion.prepareStatement("DELETE FROM circuitos WHERE id=?");
           ps.setInt(1,idCircuito);
           ps.execute();           
           return true;
       }catch(SQLException e){
           System.out.print(e.toString());
           return false;
       }
    }
   
}
