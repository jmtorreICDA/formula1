/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Circuito;
import modelo.ModeloDatos;
import modelo.Coche;

/**
 *
 * @author jmdel
 */
@WebServlet(name = "Cochescircuito", urlPatterns = {"/Cochescircuito"})
public class Cochescircuito extends HttpServlet {
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModeloDatos bd = new ModeloDatos();//ejecuta el constructor y conecta con la bbdd
        String urlUsuario;
        RequestDispatcher dispatcher = null;
        urlUsuario = request.getParameter("urlUsuario");
        
        if (urlUsuario == null || urlUsuario.isEmpty()){//la dirección base muestra página principal
            dispatcher = request.getRequestDispatcher("Ganancia/index.jsp");////pa´gina principal con listado de circuitos  y coches a elegir
            List<Coche> listaCoches = bd.mostrarCoches(); //genera el listado de coches
            request.setAttribute("listacoches", listaCoches);//lo almacena el variable de sesión para recuperarlo desde index.jsp
            List<Circuito> listaCircuitos = bd.mostrarCicuitos();//genera el listado de circuitos
            request.setAttribute("listacircuitos", listaCircuitos);//lo almacen en la variable de sesión para recuperarlo desde index.jsp
            
        }else if ("nuevoCoche".equals(urlUsuario)){//muestra formulario nuevo coche
            dispatcher = request.getRequestDispatcher("Ganancia/nuevoCoche.jsp");
            
        }else if ("insertarCoche".equals(urlUsuario)){//codigo de insertar coche y vuelta a index.jsp
            String nombre= request.getParameter("nombre"); //coge el parámetro del formulario
            int ganancia= Integer.parseInt(request.getParameter("ganancia"));
            Coche coche= new Coche(0,nombre,ganancia);//crea uh objeto coche
            bd.insertarCoche(coche);//lo inserta en la base de datos
            //Ahora se generan los listados de nuevo para mostarlos en index.jsp
            dispatcher = request.getRequestDispatcher("Ganancia/index.jsp");//fija la direccion de reenvio
            List<Coche> listaCoches = bd.mostrarCoches(); //genera el listado de coches
            request.setAttribute("listacoches", listaCoches);//lo almacena el variable de sesión para recuperarlo desde index.jsp
            List<Circuito> listaCircuitos = bd.mostrarCicuitos();//genera el listado de circuitos
            request.setAttribute("listacircuitos", listaCircuitos);//lo almacena en la variable de sesión para recuperarlo desde index.jsp    
            
        } else if ("modificarCoche".equals(urlUsuario)){ //muestra  al formulario modificarCoche
            dispatcher = request.getRequestDispatcher("Ganancia/modificarCoche.jsp");//reenvia al formulario para modificar el coche
            int id= Integer.parseInt(request.getParameter("id")); //coge el parámetro del formulario modificar
            Coche coche = bd.buscarCoche(id);//alamcena todos los datos del coche a modificar en un objeto coche
            request.setAttribute("coche",coche); //almacena en la variable de sesión el coche para mostrarlo en modificarCoche.jsp
            
        } else if ("actualizarCoche".equals(urlUsuario)){//codigo de modificar coche y vuelta a index.jsp
            int id= Integer.parseInt(request.getParameter("id")); //coge el parámetro del formulario
            String nombre= request.getParameter("nombre"); //coge el parámetro del formulario
            int ganancia= Integer.parseInt(request.getParameter("ganancia"));
            Coche coche= new Coche(id,nombre,ganancia);//crea uh objeto coche
            bd.actualizarCoche(coche);//lo inserta en la base de datos
            //Ahora se generan los listados de nuevo para mostarlos en index.jsp
            dispatcher = request.getRequestDispatcher("Ganancia/index.jsp");//fija la direccion de reenvio
            List<Coche> listaCoches = bd.mostrarCoches(); //genera el listado de coches
            request.setAttribute("listacoches", listaCoches);//lo almacena el variable de sesión para recuperarlo desde index.jsp
            List<Circuito> listaCircuitos = bd.mostrarCicuitos();//genera el listado de circuitos
            request.setAttribute("listacircuitos", listaCircuitos);//lo almacena en la variable de sesión para recuperarlo desde index.jsp    
            
        }else if ("eliminarCoche".equals(urlUsuario)){//codigo de eliminar coche y vuelta a index.jsp
            int id= Integer.parseInt(request.getParameter("id")); //coge el parámetro del formulario
            bd.eliminarCoche(id);//lo elimiina en la base de datos
            //Ahora se generan los listados de nuevo para mostarlos en index.jsp
            dispatcher = request.getRequestDispatcher("Ganancia/index.jsp");//fija la direccion de reenvio
            List<Coche> listaCoches = bd.mostrarCoches(); //genera el listado de coches
            request.setAttribute("listacoches", listaCoches);//lo almacena el variable de sesión para recuperarlo desde index.jsp
            List<Circuito> listaCircuitos = bd.mostrarCicuitos();//genera el listado de circuitos
            request.setAttribute("listacircuitos", listaCircuitos);//lo almacena en la variable de sesión para recuperarlo desde index.jsp 
            
        }else if ("nuevoCircuito".equals(urlUsuario)){//muestra formulario nuevo circuito
            dispatcher = request.getRequestDispatcher("Ganancia/nuevoCircuito.jsp");    
            
        }else if ("insertarCircuito".equals(urlUsuario)){//codigo de insertar nuevo circuito y vuelta a index.jsp
            //Captura los datos introducidos por el usuario en el formulario nuevoCircuito.jsp
            String nombre= request.getParameter("nombre"); 
            String ciudad= request.getParameter("ciudad");
            String pais = request.getParameter("pais");
            int vueltas= Integer.parseInt(request.getParameter("vueltas"));
            int longitud= Integer.parseInt(request.getParameter("longitud"));
            int curvas= Integer.parseInt(request.getParameter("curvas"));
            
            //Inserta la nueva información en la bbdd
            Circuito circuito=new Circuito(0,nombre,ciudad,pais,vueltas,longitud,curvas);//crea uh objeto circuito, la bbdd le pone el indice que le toque            
            bd.insertarCircuito(circuito);//lo inserta en la base de datos
            
            //Ahora se generan los listados de nuevo para mostarlos en index.jsp
            dispatcher = request.getRequestDispatcher("Ganancia/index.jsp");//fija la direccion de reenvio
            List<Coche> listaCoches = bd.mostrarCoches(); //genera el listado de coches
            request.setAttribute("listacoches", listaCoches);//lo almacena el variable de sesión para recuperarlo desde index.jsp
            List<Circuito> listaCircuitos = bd.mostrarCicuitos();//genera el listado de circuitos
            request.setAttribute("listacircuitos", listaCircuitos);//lo almacena en la variable de sesión para recuperarlo desde index.jsp 
            
        } else if ("modificarCircuito".equals(urlUsuario)){ //muestra  al formulario modificarCircuito
            dispatcher = request.getRequestDispatcher("Ganancia/modificarCircuito.jsp");//reenvia al formulario para modificar el circuito
            int id= Integer.parseInt(request.getParameter("id")); //coge el parámetro del formulario modificar
            Circuito circuito=bd.buscarCircuito(id);//busca el circuito en la bbdd
            request.setAttribute("circuito",circuito); //almacena en la variable de sesión el circuito para mostrarlo en modificarCircuito.jsp
            
        } else if ("actualizarCircuito".equals(urlUsuario)){//codigo de modificar actualizarCircuito y vuelta a index.jsp
                //Capturar los parámetros introducidos por el usuario en el formulario modificarCircutio.jsp
            int id= Integer.parseInt(request.getParameter("id")); 
            String nombre= request.getParameter("nombre");
            String ciudad= request.getParameter("ciudad");
            String pais = request.getParameter("pais");
            int vueltas= Integer.parseInt(request.getParameter("vueltas"));
            int longitud= Integer.parseInt(request.getParameter("longitud"));
            int curvas= Integer.parseInt(request.getParameter("curvas"));            
            
            //Actualiza la bbdd con la nueva información
            Circuito circuito=new Circuito(id,nombre,ciudad,pais,vueltas,longitud,curvas);//crea uh objeto circuito, esta vez ponemos el id del circuito
            bd.actualizarCircuito(circuito);//lo inserta en la base de datos actualizando los datos que había.
            
            //Ahora se generan los listados de nuevo para mostarlos en index.jsp
            dispatcher = request.getRequestDispatcher("Ganancia/index.jsp");//fija la direccion de reenvio
            List<Coche> listaCoches = bd.mostrarCoches(); //genera el listado de coches
            request.setAttribute("listacoches", listaCoches);//lo almacena el variable de sesión para recuperarlo desde index.jsp
            List<Circuito> listaCircuitos = bd.mostrarCicuitos();//genera el listado de circuitos
            request.setAttribute("listacircuitos", listaCircuitos);//lo almacena en la variable de sesión para recuperarlo desde index.jsp
            
        }else if ("eliminarCircuito".equals(urlUsuario)){//codigo de eliminar circuito y vuelta a index.jsp
                //Capturar el id del circuito a eliminar y se elimina de la bbdd
            int id= Integer.parseInt(request.getParameter("id")); //coge el parámetro del formulario
            bd.eliminarCircuito(id);//lo elimina en la base de datos
            
            //Ahora se generan los listados de nuevo para mostarlos en index.jsp
            dispatcher = request.getRequestDispatcher("Ganancia/index.jsp");//fija la direccion de reenvio
            List<Coche> listaCoches = bd.mostrarCoches(); //genera el listado de coches
            request.setAttribute("listacoches", listaCoches);//lo almacena el variable de sesión para recuperarlo desde index.jsp
            List<Circuito> listaCircuitos = bd.mostrarCicuitos();//genera el listado de circuitos
            request.setAttribute("listacircuitos", listaCircuitos);//lo almacena en la variable de sesión para recuperarlo desde index.jsp 
                                    
        }else if ("calcularGanancia".equals(urlUsuario)){
            int idCircuito=Integer.parseInt(request.getParameter("rbcircuito"));//almacena value del radio seleccionado que es el indice del circuito
            int idCoche= Integer.parseInt(request.getParameter("rbcoche"));//almacena value del radio seleccionado que es el indice del coche
            Coche coche=bd.buscarCoche(idCoche);
            Circuito circuito=bd.buscarCircuito(idCircuito);
            int ganancia = coche.getGanancia() * circuito.getCurvas() * circuito.getVueltas();
            request.setAttribute("ganancia",ganancia);
            request.setAttribute("nombreCoche",coche.getNombre());
            request.setAttribute("nombreCircuito",circuito.getNombre());
            dispatcher = request.getRequestDispatcher("Ganancia/verGanancia.jsp");//fija la direccion de reenvio
            
            List<Coche> listaCoches = bd.mostrarCoches(); //genera el listado de coches
            request.setAttribute("listacoches", listaCoches);//lo almacena el variable de sesión para recuperarlo desde index.jsp
            List<Circuito> listaCircuitos = bd.mostrarCicuitos();//genera el listado de circuitos
            request.setAttribute("listacircuitos", listaCircuitos);//lo almacena en la variable de sesión para recuperarlo desde index.jsp 
            
       
        } else {//en caso de que no ejecute nigún método mostramos la vista principal
            dispatcher = request.getRequestDispatcher("Ganancia/index.jsp");//fija la direccion de reenvio
            List<Coche> listaCoches = bd.mostrarCoches(); //genera el listado de coches
            request.setAttribute("listacoches", listaCoches);//lo almacena el variable de sesión para recuperarlo desde index.jsp
            List<Circuito> listaCircuitos = bd.mostrarCicuitos();//genera el listado de circuitos
            request.setAttribute("listacircuitos", listaCircuitos);//lo almacena en la variable de sesión para recuperarlo desde index.jsp   
        }
        dispatcher.forward(request,response);//redirecciona a index.jsp
     
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             doGet(request,response);//todas las peticiones las gestiona doGet()
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
