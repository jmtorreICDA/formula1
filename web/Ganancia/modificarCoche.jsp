<%-- 
    Document   : modificarCoche
    Created on : 07-feb-2021, 2:18:55
    Author     : jmdel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calcular Ganancia de Potencias</title>
    </head>
    <body>
        <h1>Modificar Coche</h1>
        <form action="Cochescircuito?urlUsuario=actualizarCoche" method="POST" autocomplete ="off">
            <input id="id" name="id" type="hidden" value="<c:out value="${coche.id}" />"  />
            <p>
                Nombre :
                <input id="nombre" name="nombre" type="text" value="<c:out value="${coche.nombre}" />" required/>                
            </p>
            <p>
                Ganancia Kw :
                <input id="ganancia" name="ganancia" type="number" min="4" max=10 value="<c:out value="${coche.ganancia}" />" required/>               
            </p>
            <button id="guardar" name="guardar" type="submit">Guardar</button>
    </body>
</html>
