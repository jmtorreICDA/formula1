
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calcular Ganancia de Potencias</title>
    </head>
    <body>
        <h1>Modificar Circuito</h1>
        <form action="Cochescircuito?urlUsuario=actualizarCircuito" method="POST" autocomplete ="off">
            <input id="id" name="id" type="hidden" value="<c:out value="${circuito.id}" />"  />
            <p>
                Nombre :
                <input id="nombre" name="nombre" type="text" value="<c:out value="${circuito.nombre}" />" required/>                
            </p>
            <p>
                Ciudad :
                <input id="ciudad" name="ciudad" type="text" value="<c:out value="${circuito.ciudad}" />" required/>                
            </p>
            <p>
                País :
                <input id="pais" name="pais" type="text" value="<c:out value="${circuito.pais}" />" required/>                
            </p>
            <p>
                Número de vueltas :
                <input id="vueltas" name="vueltas" type="number"  min="40" max="80" value="<c:out value="${circuito.vueltas}" />" required/>               
            </p>
             <p>
                Longitud (metros) :
                <input id="longitud" name="longitud" type="number" min="3000" max="9000 "value="<c:out value="${circuito.longitud}" />" required/>               
            </p>
             <p>
                Número de curvas :
                <input id="curvas" name="curvas" type="number" min="6" max="20" value="<c:out value="${circuito.curvas}" />" required/>               
            </p>
            <button id="guardar" name="guardar" type="submit">Guardar</button>
    </body>
</html>
