

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calcular Ganancia de Potencias</title>
    </head>
    <body>
        <h1>Nuevo Circuito</h1>
        <br /><br />
        <form action="Cochescircuito?urlUsuario=insertarCircuito" method="POST" autocomplete ="off">
            <p>
                Nombre :
                <input id="nombre" name="nombre" type="text" required/>                
            </p>
            <p>
                Ciudad :
                <input id="ciudad" name="ciudad" type="text" required/>                
            </p>
            <p>
                País :
                <input id="pais" name="pais" type="text" required/>                
            </p>
            <p>
                Número de Vueltas :
                <input id="vueltas" name="vueltas" type="number" min="40" max="80" required/>               
            </p>
            <p>
                Longitud (metros) :
                <input id="longitud" name="longitud" type="number" min="3000" max="9000" required/>               
            </p>
            <p>
                Número de Curvas :
                <input id="curvas" name="curvas" type="number" min="6" max="20" required/>               
            </p>
            <button id="guardar" name="guardar" type="submit" >Guardar</button>
    </body>
</html>
