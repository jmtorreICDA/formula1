<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Calcular Ganancia de Potencias</title>
        
    </head>
    <body class="resultado">
        
        <h1>Ganancia de potencia</h1>
        <hr>    
        <table border =1 >
            <thead>
                <th id="thcoche">Nombre Coche</th>
                <th id="thcircuito">Nombre Circuito</th>
                <th id="thganancia">Ganancia Potencia (Kw)</th>
            </thead>
            <tbody>

            <tr>
                <td >${nombreCoche}</td>
                <td >${nombreCircuito}</td>
                <td >${ganancia}</td>
            </tr>

        </tbody>
        </table>
        
        <br>
        <br> <a href="Cochescircuito?urlUsuario="> Ir al comienzo</a>
    </body>
</html>
