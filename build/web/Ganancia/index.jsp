

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calcular Ganancia de Potencias</title>      
    </head>
    <body>
      <form action="Cochescircuito?urlUsuario=calcularGanancia" method="POST">
        <h1>Ganancia de Potencias</h1>
        
        <br />
        <table border="1" width="40%">
            <thead>
                <h2>Coches a elegir</h2>
                <a href="Cochescircuito?urlUsuario=nuevoCoche">Añadir Coche</a>
                <br /> 
                <tr>
                    <th></th>
                    <th>Nombre Coche</th>
                    <th>Ganancia Kw</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="coche" items="${listacoches}">
                    <tr>
                        <td><input type="radio" name="rbcoche" checked="true" value="${coche.id}"></td>
                        <td align="center"><c:out value = "${coche.nombre}" /></td>
                        <td align="center"><c:out value = "${coche.ganancia}" /></td>
                        <td><a href="Cochescircuito?urlUsuario=modificarCoche&id=<c:out value = "${coche.id}" />">Modificar</a></td>
                        <td><a href="Cochescircuito?urlUsuario=eliminarCoche&id=<c:out value = "${coche.id}" />">Eliminar</a></td>
                        
                    </tr>
                </c:forEach>
            </tbody>            
        </table>
        <br /><br />
      
        <br />
        <table border="1" width="40%">
            <thead>
                <h2>Circuitos a elegir</h2>
                <a href="Cochescircuito?urlUsuario=nuevoCircuito">Añadir Circuito</a> 
                <br />
                <tr>
                    <th></th>
                    <th>Nombre Circuito</th>
                    <th>Ciudad</th>
                    <th>Pais</th>
                    <th>Numero Vueltas</th>
                    <th>Longitud</th>
                    <th>Numero de Curvas</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="circuito" items="${listacircuitos}">
                    <tr>
                        <td><input type="radio" name="rbcircuito" checked="true" value="${circuito.id}"></td>
                        <td align="center"><c:out value = "${circuito.nombre}" /></td>
                        <td align="center"><c:out value = "${circuito.ciudad}" /></td> 
                        <td align="center"><c:out value = "${circuito.pais}" /></td>
                        <td align="center"><c:out value = "${circuito.vueltas}" /></td>
                        <td align="center"><c:out value = "${circuito.longitud}" /></td>
                        <td align="center"><c:out value = "${circuito.curvas}" /></td>
                        <td><a href="Cochescircuito?urlUsuario=modificarCircuito&id=<c:out value = "${circuito.id}" />">Modificar</a></td>
                        <td><a href="Cochescircuito?urlUsuario=eliminarCircuito&id=<c:out value = "${circuito.id}" />">Eliminar</a></td>
                    </tr>
                </c:forEach>
            </tbody>            
        </table>
        <br />
        <h2>Pulsa para calcular la Ganancia en Kw</h2>
        <input id = "boton1" type="submit" name="B1" value="Calcular Ganancia">
      </form>         
    </body>
</html>
